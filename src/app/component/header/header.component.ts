import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    $(document).ready(function () {

      $(window).on("scroll", function () {
        if ($(window).scrollTop() >= 500) {
          $("header").addClass("header-compressed");
        } else {
          $("header").removeClass("header-compressed");
        }
      });


      
      $('#toggle').click(function () {
        $(this).toggleClass('active');
        $('#overlay').toggleClass('open');
      });
    });


    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        $("#overlay").removeClass("open");
        $("#toggle").removeClass("active");
      }
    });
  }

}
