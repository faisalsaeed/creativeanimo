import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { TweenMax, TimelineMax, Linear } from 'gsap/all';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    $(document).ready(function () {
      var body = $('body'),
        container = $('.about-tween-max'),
        title = $('.about-title'),
        img = $('.about-img');
      TweenMax.set([title, img], { transformStyle: "preserve-3d" });
      body.mousemove(function (e) {
        var sxPos = e.pageX / container.width() * 100 - 100;
        var syPos = e.pageY / container.height() * 100 - 100;
        TweenMax.to(title, 2, {
          rotationY: 0.03 * sxPos,
          rotationX: -0.03 * syPos,
          transformPerspective: 1000,
          transformOrigin: "center center -200",

        });
        TweenMax.to(img, 2, {
          rotationY: 0.03 * sxPos,
          rotationX: -0.03 * syPos,
          transformPerspective: 1000,
          transformOrigin: "center center -200",

        });
      });
    });


    $(window).mousemove(function(e) {     
      $(".cursor").css({
        left: e.pageX,
        top: e.pageY
      })    
    })
    $(".cursor-link")
      .on("mouseenter", function() {   
      $('.cursor').addClass("active")   
    })
    .on("mouseleave", function() {    
      $('.cursor').removeClass("active")    
    })





  }

}
