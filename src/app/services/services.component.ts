import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { TweenMax, TimelineMax, Linear } from 'gsap/all';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    $(document).ready(function () {
      var body = $('body'),
        container = $('.services-tween-max'),
        title = $('.services-title'),
        img = $('.services-img');
      TweenMax.set([title, img], { transformStyle: "preserve-3d" });
      body.mousemove(function (e) {
        var sxPos = e.pageX / container.width() * 100 - 100;
        var syPos = e.pageY / container.height() * 100 - 100;
        TweenMax.to(title, 2, {
          rotationY: 0.03 * sxPos,
          rotationX: -0.03 * syPos,
          transformPerspective: 1000,
          transformOrigin: "center center -200",

        });
        TweenMax.to(img, 2, {
          rotationY: 0.03 * sxPos,
          rotationX: -0.03 * syPos,
          transformPerspective: 1000,
          transformOrigin: "center center -200",

        });
      });
    });


    $('.button').click(function () {
      var buttonId = $(this).attr('id');
      $('#modal-container').removeAttr('class').addClass(buttonId);
      $('body').addClass('modal-active');
    })

    $('#modal-container').click(function () {
      $(this).addClass('out');
      $('body').removeClass('modal-active');
    });

    $( ".modal-body-content" ).click(function( event ) {
      event.stopPropagation();
      // Do something
    });





    var $tickerWrapper = $(".tickerwrapper");
    var $list = $tickerWrapper.find("ul.list");
    var $clonedList = $list.clone();
    var listWidth = 10;

    $list.find("li").each(function (i) {
      listWidth += $(this, i).outerWidth(true);
    });

    var endPos = $tickerWrapper.width() - listWidth;

    $list.add($clonedList).css({
      "width": listWidth + "px"
    });

    $clonedList.addClass("cloned").appendTo($tickerWrapper);

    //TimelineMax
    var infinite = new TimelineMax({ repeat: -1, paused: true });
    var time = 100;

    infinite
      .fromTo($list, time, { rotation: 0.01, x: 0 }, { force3D: true, x: -listWidth, ease: Linear.easeNone }, 0)
      .fromTo($clonedList, time, { rotation: 0.01, x: listWidth }, { force3D: true, x: 0, ease: Linear.easeNone }, 0)
      .set($list, { force3D: true, rotation: 0.01, x: listWidth })
      .to($clonedList, time, { force3D: true, rotation: 0.01, x: -listWidth, ease: Linear.easeNone }, time)
      .to($list, time, { force3D: true, rotation: 0.01, x: 0, ease: Linear.easeNone }, time)
      .progress(1).progress(0)
      .play();

    //Pause/Play		
    $tickerWrapper.on("mouseenter", function () {
      infinite.pause();
    }).on("mouseleave", function () {
      infinite.play();
    });



  
  }

}
